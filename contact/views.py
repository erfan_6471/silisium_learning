from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, CreateView, UpdateView

from account.mixin import AuthorLogin, FieldsContactMixin, FormContactValiMixin, FieldsContactDetailMixin, \
    FormContactDetailValiMixin, ContactDeleteAccessMixin
from contact.models import Contact, DetailContact


class ContactList(AuthorLogin, LoginRequiredMixin, ListView):
    template_name = "registration/contact_list.html"
    context_object_name = "contacts"
    queryset = Contact.objects.all()


class ContactCreate(AuthorLogin, LoginRequiredMixin, FieldsContactMixin, FormContactValiMixin, CreateView):
    model = Contact
    template_name = "registration/contact-create-update.html"


#
class ContactUpdate(LoginRequiredMixin, FieldsContactMixin, FormContactValiMixin, UpdateView):
    model = Contact
    template_name = "registration/contact-create-update.html"


# contactlist


class ContactListView(AuthorLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        slug = self.kwargs.get("slug")
        detail_contacts = DetailContact.objects.filter(contact_object__slug=slug)
        return detail_contacts

    template_name = "registration/contact_detail.html"


class ContactProfileDetailView(AuthorLogin, LoginRequiredMixin, DeleteView):
    context_object_name = "contact"

    def get_object(self):
        pk = self.kwargs.get("pk")
        return get_object_or_404(DetailContact, pk=pk)

    template_name = "registration/contact_detail_profile.html"


class ContactDetailCreate(AuthorLogin, LoginRequiredMixin, FieldsContactDetailMixin, FormContactDetailValiMixin,
                          CreateView):
    model = DetailContact
    template_name = "registration/contact-detail-create-update.html"


class ContactDetailUpdate(AuthorLogin, LoginRequiredMixin, FieldsContactDetailMixin, FormContactDetailValiMixin,
                          UpdateView):
    model = DetailContact
    template_name = "registration/contact-detail-create-update.html"


class ContactDelete(ContactDeleteAccessMixin, DeleteView):
    model = DetailContact
    success_url = reverse_lazy('contact:contact_list')
    template_name = "registration/contact-confirm-delete.html"
