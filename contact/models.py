from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from shortuuidfield import ShortUUIDField
from taggit.managers import TaggableManager

from account.models import User
from comment.models import Comment
from extensions.utils import Jalali_covertor


class Contact(models.Model):
    uuid = ShortUUIDField(verbose_name="کد اختصاصی", unique=True)
    slug = models.SlugField(verbose_name="اسلاگ شرکت", max_length=100, unique=True)
    company_name = models.CharField(verbose_name="نام شرکت", max_length=30)
    creator = models.ForeignKey(User, verbose_name="ایجاد کننده", null=True, on_delete=models.CASCADE,
                                limit_choices_to={'is_active': True})
    created_on = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "شرکت حمل "
        verbose_name_plural = "شرکت های حمل"

    def __str__(self):
        return self.company_name

    def get_absolute_url(self):
        return reverse("contact:contact_list")

    def Jpublish(self):
        return Jalali_covertor(self.created_on)

    Jpublish.short_description = "آخرین تماس با مشتری"


class DetailContact(models.Model):
    PHONE_NUMBER_TYPE_CHOICES = (
        ('home', 'Home'),
        ('mobile', 'Mobile'),
        ('fax', 'Fax'),
        ('work', 'Work'),
        ('other', 'Other')
    )
    EMAIL_TYPE_CHOICES = (
        ('private', 'private'),
        ('work', 'Work'),
        ('other', 'Other')
    )
    SHIPMENT_CHOICES = (('TT', 'TT'),  # truck
                        ('CC', 'CC'),  # ocean
                        ('AA', 'AA'),  # air
                        ('TR', 'TR'),)  # transit
    first_name = models.CharField(null=True, max_length=30, verbose_name="نام")
    last_name = models.CharField(max_length=30, verbose_name="نام خانوادگی")
    phone_type = models.CharField(max_length=10, choices=PHONE_NUMBER_TYPE_CHOICES, verbose_name="نوع شماره تماس")
    phone_number = PhoneNumberField(max_length=50, verbose_name="شماره تفن")
    email_type = models.CharField(max_length=10, choices=EMAIL_TYPE_CHOICES, verbose_name="نوع ایمیل")
    email_address = models.EmailField(max_length=255, db_index=True, verbose_name="آدرس ایمیل")
    contact_object = models.ForeignKey(Contact, verbose_name="شرکت در حال فعالت", null=True,
                                       related_name='contact_phone_number',
                                       on_delete=models.CASCADE)
    author = models.ForeignKey(User, null=True,
                               related_name='creator_contact',
                               on_delete=models.CASCADE, verbose_name="ایجاد کننده")
    kind_shipment = models.CharField(verbose_name="حیطه فعالیت", max_length=2, choices=SHIPMENT_CHOICES)
    tags = TaggableManager()
    is_active = models.BooleanField(default=True, verbose_name="آیا این شخص  فغال می باشد ؟")
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    comments = GenericRelation(Comment)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

    class Meta:
        db_table = 'contacts_detail'
        verbose_name = "اطلاعات تماس با شرکت های حمل "
        verbose_name_plural = "اطلاعات تماس  با شرکت های حمل"
        ordering = ["-is_active", "date_modified"]

    def get_absolute_url(self):
        return reverse("contact:contact_list")
