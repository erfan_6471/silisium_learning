from django.urls import path

from contact.views import ContactList, ContactProfileDetailView, ContactListView, ContactCreate, ContactUpdate, \
    ContactDetailCreate, ContactDetailUpdate, ContactDelete

app_name = "contact"

urlpatterns = [path("contactlist/", ContactList.as_view(), name="contact_list"),
               path('detail/<slug:slug>', ContactListView.as_view(), name="contact_detail"),
               path('detail/profile/<int:pk>', ContactProfileDetailView.as_view(),
                    name="contact_detail_profile"),
               path('create/company', ContactCreate.as_view(), name='contact-create'),
               path('update/company/<int:pk>', ContactUpdate.as_view(), name='contact_update'),
               path('create/contact_detail', ContactDetailCreate.as_view(), name='contact_detail_create'),
               path('update/contact_detail/<int:pk>', ContactDetailUpdate.as_view(), name='contact_detail_update'),
               path('contact_detail/delete/<int:pk>', ContactDelete.as_view(), name='contact_delete'),
               ]
