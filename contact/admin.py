from django.contrib import admin

from contact.models import DetailContact, Contact

admin.site.register(Contact)
admin.site.register(DetailContact)