from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django_countries.fields import CountryField
from djmoney.models.fields import MoneyField
from phonenumber_field.modelfields import PhoneNumberField

from extensions.utils import Jalali_covertor


class User(AbstractUser):
    is_author = models.BooleanField(default=False, verbose_name="وضعیت نویسندگی")
    special_user = models.DateTimeField(verbose_name="کاربر ویژه تا:", default=timezone.now)
    email = models.EmailField(unique=True, verbose_name="ایمیل")

    def is_special_user(self):
        if self.special_user > timezone.now():
            return True
        else:
            return False

    is_special_user.boolean = True
    is_special_user.short_description = "وضعیت کاربر ویژه"


class ClientList(models.Model):
    companyName = models.CharField(max_length=100, unique=True, verbose_name="نام شرکت")
    firstName = models.CharField(max_length=100, verbose_name="نام")
    lastName = models.CharField(max_length=100, verbose_name="نام خانوادگی")
    phoneNumber = PhoneNumberField(unique=True, null=False, blank=False, verbose_name="شماره موبایل")  # Here
    telephoneNumber = PhoneNumberField(null=True, blank=True, verbose_name="شماره تلفن شرکت")  # Here
    email = models.EmailField(unique=True, verbose_name="ایمیل")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    publish = models.DateTimeField(verbose_name="آخرین تماس با مشتری", default=timezone.now)
    status = models.BooleanField(verbose_name="آیا جزو مشتری های فعال می باشد؟", default=True)
    position = models.IntegerField(verbose_name="اولویت")
    sales = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="clientlist",
                              verbose_name="بازاریاب",
                              limit_choices_to={'is_author': True})
    description = models.TextField(null=True, blank=True, verbose_name="توضیحات", )

    class Meta:
        verbose_name = "لیست مشتری"
        verbose_name_plural = "لیست مشتریان"
        ordering = ["-status", "position", "publish"]

    def __str__(self):
        return self.companyName

    def get_absolute_url(self):
        return reverse("account:client_list")

    def Jpublish(self):
        return Jalali_covertor(self.publish)

    Jpublish.short_description = "آخرین تماس با مشتری"


class ReferenceList(models.Model):
    STATUS_CHOICES = (('d', 'تایید نشده'),  # not confirm
                      ('i', 'در حال بررسی'),  # investigation
                      ('p', 'تایید و در حال انجام '),  # confirm and not complete
                      ('e', 'تایید و به پایان رسیده ( سود دریافت نشده ) '),  # confirm and complete (not paid)
                      ('f', 'بایگانی'),  # finished
                      )
    SHIPMENT_CHOICES = (('TT', 'TT'),  # truck
                        ('CC', 'CC'),  # ocean
                        ('AA', 'AA'),  # air
                        ('TR', 'TR'),)  # transit
    reference_number = models.CharField(verbose_name="شماره رفرنس", max_length=20, unique=True)
    companyName = models.ForeignKey(ClientList, null=True, on_delete=models.CASCADE,
                                    related_name="reference_list",
                                    verbose_name="مشتری",
                                    limit_choices_to={'status': True})
    kind_shipment = models.CharField(verbose_name="نوع سفر", max_length=2, choices=SHIPMENT_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    start_project = models.DateTimeField(verbose_name="زمان شروع حمل", default=timezone.now)
    position = models.IntegerField(verbose_name="اولویت")
    description = models.TextField(null=True, blank=True, verbose_name="توضیحات", )
    country_of_receipt = CountryField(verbose_name="کشور بارگیری محموله")
    port_of_receipt = models.CharField(verbose_name="پورت بارگیری محموله", max_length=200)
    net_rate = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='USD', verbose_name="نرخ خرید")
    selling_rate = MoneyField(max_digits=14, null=True, decimal_places=2, default_currency='USD',
                              verbose_name="نرخ فروش")
    country_of_discharge = CountryField(verbose_name="کشور مقصد")
    port_of_discharge = models.CharField(verbose_name="گمرک مقصد", max_length=200)
    status = models.CharField(verbose_name="وضعیت رفرنس", max_length=1, choices=STATUS_CHOICES)

    class Meta:
        verbose_name = "رفرنس بازاریاب "
        verbose_name_plural = "رفرنس های بازاریاب"
        ordering = ["-created"]

    def __str__(self):
        return self.reference_number

    def profit(self):
        if self.net_rate.currency == self.selling_rate.currency:
            x=(self.selling_rate - self.net_rate) / 4
            return x
        else:
            return "واحد پول باید یکسان باشد"

    def Jpublish(self):
        return Jalali_covertor(self.start_project)

    def get_absolute_url(self):
        return reverse("account:reference_list")

    Jpublish.short_description = "زمان شروع حمل"
