from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ngettext

from .models import User, ClientList, ReferenceList

UserAdmin.fieldsets[2][1]["fields"] = ('is_active',
                                       'is_staff',
                                       'is_superuser',
                                       'is_author',
                                       'special_user',
                                       'groups',
                                       'user_permissions')

UserAdmin.list_display += ('is_special_user',
                           'is_author')
admin.site.register(User, UserAdmin)


@admin.action(description='فعال کردن مشتری انتخاب شده')
def make_active(modeladmin, request, queryset):
    queryset.update(status=True)
    updated = queryset.update(status=True)
    modeladmin.message_user(request, ngettext(
        '%d دسته بندی فعال شد.',
        '%d دسته بندی ها فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='غیر فعال شدن دسته بندی انتخاب شده')
def make_deactivate(modeladmin, request, queryset):
    queryset.update(status=False)
    updated = queryset.update(status=False)
    modeladmin.message_user(request, ngettext(
        '%d مشتری غیر فعال شد.',
        '%d مشتری ها غیر فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


class ClientListAdmin(admin.ModelAdmin):
    list_display = ("companyName", "firstName", "lastName", "sales", "status", "phoneNumber")
    list_filter = (("status",))
    search_fields = ("companyName", "firstName", "lasttName")
    ordering = ("-status", "-position", "-publish")
    actions = [make_deactivate, make_active]


admin.site.register(ClientList, ClientListAdmin)


class ReferenceListAdmin(admin.ModelAdmin):
    list_display = (
        "reference_number", "companyName", "kind_shipment", "country_of_receipt", "port_of_discharge")
    list_filter = (("status", "kind_shipment", "start_project",))
    search_fields = ("companyName", "firstName", "lasttName")
    ordering = ("-kind_shipment", "-position", "-start_project")


admin.site.register(ReferenceList, ReferenceListAdmin)

