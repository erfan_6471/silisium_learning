from django.http import Http404
from django.shortcuts import get_object_or_404, redirect

from account.models import ClientList, ReferenceList
from blog.models import Article


class FieldsMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["title", "slug", "description", "thumbnail", "is_special", "publish",
                       "category", "is_special", "status"]
        if request.user.is_superuser:
            self.fields.append("author")

        return super().dispatch(request, *args, **kwargs)


class FieldsClientMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["companyName", "firstName", "lastName", "phoneNumber", "telephoneNumber", "email",
                       "publish", "status", "position", "description"]
        if request.user.is_superuser:
            self.fields.append("sales")

        return super().dispatch(request, *args, **kwargs)


class FormValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
            if not self.obj.status in ["d", "i"]:
                self.obj.status = "d"
        return super().form_valid(form)


class FormClientValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.sales = self.request.user
        return super().form_valid(form)


class AuthorAccessMixin():
    def dispatch(self, request, pk, *args, **kwargs):
        article = get_object_or_404(Article, pk=pk)
        if article.author == request.user and article.status in ["b", "d"] or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class AuthorClientAccessMixin():
    def dispatch(self, request, pk, *args, **kwargs):
        client = get_object_or_404(ClientList, pk=pk)
        if client.sales == request.user or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class AuthorLogin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_author or request.user.is_superuser:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:profile")
        else:
            return redirect("login")


class SuperUserAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class ClientDeleteAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_author or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class AuthorReferenceAccessMixin():
    def dispatch(self, request, pk, *args, **kwargs):
        reference = get_object_or_404(ReferenceList, pk=pk)
        if reference.companyName.sales == request.user or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")


class FieldsReferenceMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["reference_number", "kind_shipment", "start_project", "position", "description",
                       "country_of_receipt", "port_of_receipt", "country_of_discharge", "port_of_discharge", "net_rate",
                       "selling_rate", "status"]
        if request.user.is_superuser:
            self.fields.append("companyName")

        return super().dispatch(request, *args, **kwargs)


class FieldsContactMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["company_name", "slug"]
        if request.user.is_superuser:
            self.fields.append("creator")

        return super().dispatch(request, *args, **kwargs)


class FormContactValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.creator = self.request.user
        return super().form_valid(form)


##################
class FieldsContactDetailMixin():

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["first_name", "last_name", "phone_type", "phone_number", "email_type", "email_address",
                       "contact_object", "kind_shipment", "tags", "is_active"]
        if request.user.is_superuser:
            self.fields.append("author")

        return super().dispatch(request, *args, **kwargs)


class FormContactDetailValiMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
        return super().form_valid(form)


class ContactDeleteAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("you can't see this page.")
