from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView)

from account.forms import ProfileForm, ReferenceForm
from account.mixin import (FieldsMixin,
                           FormValiMixin,
                           AuthorAccessMixin,
                           SuperUserAccessMixin, AuthorLogin, FieldsClientMixin, FormClientValiMixin,
                           AuthorClientAccessMixin, ClientDeleteAccessMixin, FieldsReferenceMixin,
                           AuthorReferenceAccessMixin)
from account.models import User, ClientList, ReferenceList
from blog.models import Article
from .forms import SignupForm
from .tokens import account_activation_token


# @login_required
# def home(request):
#     return render(request, "registration/home.html")


class ArticleList(AuthorLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Article.objects.all()
        else:
            return Article.objects.filter(author=self.request.user)

    template_name = "registration/home.html"


class ArticleCreate(AuthorLogin, LoginRequiredMixin, FieldsMixin, FormValiMixin, CreateView):
    model = Article
    template_name = "registration/article-create-update.html"


class ArticleUpdate(LoginRequiredMixin, AuthorAccessMixin, FieldsMixin, FormValiMixin, UpdateView):
    model = Article
    template_name = "registration/article-create-update.html"


class Profile(LoginRequiredMixin, UpdateView):
    model = User
    form_class = ProfileForm
    template_name = "registration/profile.html"
    success_url = reverse_lazy("account:profile")

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super(Profile, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ArticleDelete(SuperUserAccessMixin, DeleteView):
    model = Article
    success_url = reverse_lazy('account:home')
    template_name = "registration/article-confirm-delete.html"


class Login(LoginView):
    redirect_authenticated_user = "account:home"

    def get_success_url(self):
        user = self.request.user
        if user.is_superuser or user.is_author:
            return reverse_lazy("account:home")
        else:
            return reverse_lazy("account:profile")


class Register(CreateView):
    form_class = SignupForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(self.request)
        mail_subject = 'فعال سازی اکانت'
        message = render_to_string('registration/activate_account.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
            'token': account_activation_token.make_token(user),
        })
        to_email = form.cleaned_data.get('email')
        email = EmailMessage(
            mail_subject, message, to=[to_email]
        )
        email.send()
        return HttpResponse('لینک فعال سازی به ایمیل شما ارسال شد. <a href="/login">ورود</a>')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse('اکانت شما با موفقیت فعال شد. برای ورود <a href="/login">کلیک</a> کنید.')
    else:
        return HttpResponse('لینک فعال سازی منقضی شده است. <a href="/registration">دوباره امتحان کنید.</a>')


class Client_list(AuthorLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        if self.request.user.is_superuser:
            return ClientList.objects.all()
        else:
            return ClientList.objects.filter(sales=self.request.user)

    template_name = "registration/client_list.html"


class ClientCreate(AuthorLogin, LoginRequiredMixin, FieldsClientMixin, FormClientValiMixin, CreateView):
    model = ClientList
    template_name = "registration/client-create-update.html"


#
class ClientUpdate(LoginRequiredMixin, AuthorClientAccessMixin, FieldsClientMixin, FormClientValiMixin, UpdateView):
    model = ClientList
    template_name = "registration/client-create-update.html"


class ClientDelete(ClientDeleteAccessMixin, DeleteView):
    model = ClientList
    success_url = reverse_lazy('account:client_list')
    template_name = "registration/client-confirm-delete.html"


class Reference_List(AuthorLogin, LoginRequiredMixin, ListView):
    def get_queryset(self):
        if self.request.user.is_superuser:
            return ReferenceList.objects.all()
        else:
            return ReferenceList.objects.filter(companyName__sales=self.request.user)

    template_name = "registration/reference_list.html"


class ReferenceCreate(AuthorLogin, LoginRequiredMixin, CreateView):
    model = ReferenceList
    form_class = ReferenceForm
    template_name = "registration/reference-create-update.html"

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super(ReferenceCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ReferenceUpdate(LoginRequiredMixin, AuthorReferenceAccessMixin, FieldsReferenceMixin, UpdateView):
    model = ReferenceList
    template_name = "registration/reference-create-update2.html"


class ReferenceDelete(ClientDeleteAccessMixin, DeleteView):
    model = ReferenceList
    success_url = reverse_lazy('account:reference_list')
    template_name = "registration/reference-confirm-delete.html"


def pie_chart(request):
    labels = []
    data = []

    queryset = ReferenceList.objects.filter(companyName__sales=request.user,status="e")
    for profit_rate in queryset:
        labels.append(f"{profit_rate.reference_number}  {profit_rate.net_rate.currency}")
        if profit_rate.net_rate.currency == profit_rate.selling_rate.currency:
            x=(profit_rate.selling_rate - profit_rate.net_rate) / 4
            data.append(float(x.amount))
        else:
            x="واحد گولی باید یکسان باشد"
            data.append(x)



    return render(request, 'registration/profit_monthly.html', {
        'labels': labels,
        'data': data,
    })
