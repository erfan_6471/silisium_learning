from django.urls import path

from account.views import (ArticleList,
                           ArticleCreate,
                           ArticleUpdate,
                           ArticleDelete, Profile, Client_list, ClientCreate, ClientUpdate, ClientDelete,
                           Reference_List, ReferenceCreate, ReferenceUpdate, ReferenceDelete,
                           pie_chart,
                           )

app_name = "account"

urlpatterns = [path("", ArticleList.as_view(), name="home"),
               path('article/create', ArticleCreate.as_view(), name='article-create'),
               path('article/update/<int:pk>', ArticleUpdate.as_view(), name='article-update'),
               path('article/delete/<int:pk>', ArticleDelete.as_view(), name='article-delete'),
               path('profile/', Profile.as_view(), name='profile'),
               path("clientlist/", Client_list.as_view(), name="client_list"),
               path('client/create', ClientCreate.as_view(), name='client-create'),
               path('client/update/<int:pk>', ClientUpdate.as_view(), name='client-update'),
               path('client/delete/<int:pk>', ClientDelete.as_view(), name='client-delete'),
               path("referencelist/", Reference_List.as_view(), name="reference_list"),
               path('reference/create', ReferenceCreate.as_view(), name='reference-create'),
               path('reference/update/<int:pk>', ReferenceUpdate.as_view(), name='reference-update'),
               path('reference/delete/<int:pk>', ReferenceDelete.as_view(), name='reference-delete'),
               path('pie-chart/', pie_chart, name='pie_chart'),
               ]
