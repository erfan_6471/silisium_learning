from django import forms
from django.contrib.auth.forms import UserCreationForm

from account.models import User, ReferenceList, ClientList


class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields["email"].help_text = "غیر قابل تغییر"
        if not user.is_superuser:
            self.fields["username"].disabled = True
            self.fields["special_user"].disabled = True
            self.fields["is_author"].disabled = True
            self.fields["email"].disabled = True

    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name", "special_user", "is_author"]


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class ReferenceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ReferenceForm, self).__init__(*args, **kwargs)
        self.fields['companyName'] = forms.ModelChoiceField(queryset=ClientList.objects.filter(sales=user),
                                                            label="نام شرکت")

    class Meta:
        model = ReferenceList
        fields = ["reference_number", "companyName", "kind_shipment", "start_project", "position", "description",
                  "country_of_receipt", "port_of_receipt", "country_of_discharge", "port_of_discharge","net_rate","selling_rate", "status"]


