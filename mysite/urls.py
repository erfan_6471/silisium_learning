from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, re_path
from django.urls import path

from account.views import Login, Register, activate

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('comment/', include('comment.urls')),
                  path('', include("blog.urls")),
                  path('', include("django.contrib.auth.urls")),
                  path('login/', Login.as_view(), name='login'),
                  path('register/', Register.as_view(), name='register'),
                  path(
                      'activate/<slug:uidb64>/<slug:token>/',
                      activate,
                      name='activate'),
                  path('account/', include("account.urls")),
                  path('contact/', include("contact.urls")),
                  re_path(r'^ratings/', include('star_ratings.urls', namespace='ratings')),
                  path('todo/', include('todo.urls', namespace="todo")),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
