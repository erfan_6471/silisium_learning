from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html

from account.models import User
from comment.models import Comment
from extensions.utils import Jalali_covertor


class ArticleManager(models.Manager):
    def publish(self):
        return self.filter(status='p')


class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status=True)


class Category(models.Model):
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, default=None, null=True, blank=True,
                               related_name='children', verbose_name="عبارت زیر دسته")
    title = models.CharField(verbose_name="عنوان دسته بندی", max_length=200)
    slug = models.SlugField(verbose_name="آدرس دسته بندی", max_length=100, unique=True)
    status = models.BooleanField(verbose_name="آیا نمایش داده شود؟", default=True)
    position = models.IntegerField(verbose_name="پوزیشن")

    class Meta:
        verbose_name = "دسته بندی"
        verbose_name_plural = "دسته بندی ها"
        ordering = ["parent__id", "position"]

    def __str__(self):
        return self.title

    objects = CategoryManager()


class Article(models.Model):
    STATUS_CHOICES = (('d', 'پیش نویس'),  # draft
                      ('p', 'منتشر شده'),  # publish
                      ('i', 'در حال بررسی'),  # investigation
                      ('b', 'برگشت داده شده'),)  # back
    title = models.CharField(verbose_name="موضوع", max_length=200)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="articles",
                               verbose_name="نویسنده")
    is_special = models.BooleanField(default=False, verbose_name="مقاله ویژه")
    slug = models.SlugField(verbose_name="آدرس مقاله", max_length=100, unique=True)
    description = models.TextField(verbose_name="توضیحات", )
    thumbnail = models.ImageField(verbose_name="عکس بند انگشتی", upload_to="images")
    publish = models.DateTimeField(verbose_name="زمان ایجاد", default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(verbose_name="وضعیت", max_length=1, choices=STATUS_CHOICES)
    category = models.ManyToManyField(Category, verbose_name="دسته بندی", related_name="articles")
    comments = GenericRelation(Comment)

    class Meta:
        verbose_name = "مقاله"
        verbose_name_plural = "مقالات"
        ordering = ["-publish"]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("account:home")

    def Jpublish(self):
        return Jalali_covertor(self.publish)

    Jpublish.short_description = "زمان انتشار"

    def category_publish(self):
        return self.category.filter(status=True)

    def thumbnail_tags(self):
        return format_html("<img width=80 height=60 style='border-radius: 5px' src='{}'>".format(self.thumbnail.url))

    thumbnail_tags.short_description = "عکس بند انگشتی"
    objects = ArticleManager()

    def category_to_str(self):
        return ",".join([category.title for category in self.category_publish()])

    category_to_str.short_description = "دسته بندی"




