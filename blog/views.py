from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from account.mixin import AuthorAccessMixin
from account.models import User
from blog.models import Article, Category


# object Base view
# def home(request, page=1):
#     articles_list = Article.objects.publish()
#     p = Paginator(articles_list, 4)
#     articles = p.get_page(page)
#     context = {
#         "articles": articles
#     }
#     return render(request, "blog/home.html", context)


# class Base view
class ArticleList(ListView):
    # model = Article
    template_name = "blog/home.html"
    context_object_name = "articles"
    queryset = Article.objects.publish()
    paginate_by = 3


# object Base view
# def detail(request, slug):
#     context = {
#         "article": get_object_or_404(Article.objects.publish(), slug=slug, status="p")
#
#     }
#     return render(request, "blog/detail.html", context)


# class Base view
class ArticleDetailView(DetailView):
    def get_object(self):
        slug = self.kwargs.get("slug")
        return get_object_or_404(Article.objects.publish(), slug=slug)

    template_name = "blog/detail.html"


class ArticlePreview(AuthorAccessMixin, DetailView):
    def get_object(self):
        pk = self.kwargs.get("pk")
        return get_object_or_404(Article, pk=pk)

    template_name = "blog/detail.html"


# def category(request, slug, page=1):
#     category = get_object_or_404(Category, slug=slug, status=True)
#     articles_list = category.articles.publish()
#     p = Paginator(articles_list, 3)
#     articles = p.get_page(page)
#     context = {
#         "category": category,
#         "articles": articles
#
#     }
#     return render(request, "blog/category.html", context)

class CategoryList(ListView):
    template_name = "blog/category.html"
    context_object_name = "articles"
    paginate_by = 4

    def get_queryset(self):
        global category
        slug = self.kwargs.get("slug")
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.articles.publish()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context


class AuthorList(ListView):
    template_name = "blog/author_list.html"
    paginate_by = 4
    context_object_name = "articles"

    def get_queryset(self):
        global author
        username = self.kwargs.get("username")
        author = get_object_or_404(User, username=username)
        return author.articles.publish()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = author
        return context
