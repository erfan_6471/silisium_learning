from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ngettext

from account.models import ClientList
from blog.models import Article, Category


@admin.action(description='انتشار مقالات انتخاب شده')
def make_published(modeladmin, request, queryset):
    queryset.update(status='p')
    updated = queryset.update(status='p')
    modeladmin.message_user(request, ngettext(
        '%d مقاله منتشر شد.',
        '%d مقاله منتشر شدند.',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='پیش نویس شدن مقالات انتخاب شده')
def make_draft(modeladmin, request, queryset):
    queryset.update(status='d')
    updated = queryset.update(status='d')
    modeladmin.message_user(request, ngettext(
        '%d مقاله پیش نویس شد.',
        '%d مقاله پیش نویس شدند.',
        updated,
    ) % updated, messages.SUCCESS)


admin.site.site_header = "وبلاگ عرفان"


@admin.action(description='فعال کردن دسته بندی های انتخاب شده')
def make_active(modeladmin, request, queryset):
    queryset.update(status=True)
    updated = queryset.update(status=True)
    modeladmin.message_user(request, ngettext(
        '%d دسته بندی فعال شد.',
        '%d دسته بندی ها فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='غیر فعال شدن دسته بندی انتخاب شده')
def make_deactivate(modeladmin, request, queryset):
    queryset.update(status=False)
    updated = queryset.update(status=False)
    modeladmin.message_user(request, ngettext(
        '%d دسته بندی غیر فعال شد.',
        '%d دسته بندی ها غیر فعال شدند.',
        updated,
    ) % updated, messages.SUCCESS)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("title", "position", "parent", "slug", "status")
    list_filter = (("status",))
    search_fields = ("title", "slug")
    prepopulated_fields = {"slug": ("title",)}
    actions = [make_active, make_deactivate]


admin.site.register(Category, CategoryAdmin)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "thumbnail_tags", "author", "slug", "Jpublish", "is_special", "status", "category_to_str")
    list_filter = ("publish", "status", "author")
    search_fields = ("title", "description")
    prepopulated_fields = {"slug": ("title",)}
    ordering = ("-status", "-publish")
    actions = [make_published, make_draft]


admin.site.register(Article, ArticleAdmin)



